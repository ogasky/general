Here is a checklist of things to consider when planning and hosting a summit.

Helpful resources:

- In-person [How to plan an event 10-step guide](https://guidebook.com/resources/event-planning-guide/) by Guidebook by Content Hub
- Hybrid events is to checkout is the write up by Hopin called, [Today’s Ultimate Hybrid Event Checklist](https://hopin.com/blog/hybrid-event-checklist)


**Pre- summit planning checklist:**
---

- [ ] Identify the goal and KPIs
- [ ] Will the event be only in-person or online, hybrid
        -  What is needed to make it an successful hybrid event? I.e platform? Can we use Hopin?
- [ ] Identify budget for event | Consider the following:
     - Room rental/venue
     -  Food and beverages
     -  Equipment
     -  Speaker 
     -  Travel for staff 
     -  Travel stipends for attendees
     -  Insurance if required

- [ ] Event planning timeline

- [ ] Settle on a theme and clear event messaging

- [ ] Identify your team and set clear responsibilities / roles

- [ ] Select a date & venue

- [ ] Book the venue
        
- [ ] Is AV needed? 
   - Room arrangement 
   - Signage

- [ ] Food & Beverage 

- [ ] Plan your agenda (Remember to allocate time for breaks and transitions in agenda.)

  - Identify and vet speakers

  - Activities 

  - Activations 



- [ ] Identify the details around travel for attendees
        - Book a group rate for a hotel

- [ ] Attendee corresponence

    - Create invitation  
    - Registration site 
    - Travel information (Room block, flight or transportation stipend) 
    - How will attendees register for the event
 

- [ ] Promotion - Build social assets and plan 

- [ ] Custom event Swag: Contact Suri Patel
        - Send swag to hotel that DRI will be staying 

**Day of event:**
---
- [ ] Arrive early to double check space, set up for AV and all other logistics to ensure a smooth event
- [ ] Set up room for guest arrival
    - Arrange swag 
- [ ] Check-in table for attendees 

**Post-event:**
---
- [ ] Create a feedback survey (Follow up on attendees experience)
- 
