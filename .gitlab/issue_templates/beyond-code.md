

Please use this template when proposing a topic for Beyond Code on TwitterSpace conversation.

 >> Title each issue as: Beyond Code Conversation Proposal - MM.DD.YY 

It is important to use platforms that our audience is heavily using organically. Twitter spaces are a new fresh way to meet our audience where they are. These conversations are to be casual, informative and fun. 

A bit of context from Twitter explaining [Twitter spaces - Spaces](https://help.twitter.com/en/using-twitter/spaces) is a new way to have live audio conversations on Twitter. We love how it’s shaping up, but there is much more to come including new features and updates.

**Goal:** engage and increase participation from the wider community with conversations that give in-depth insight into all things beyond code within GitLab and externally. 

Each Twitter Space convo will have a runtime between 40 mins to an (1) hour.

Planning  Twitter Space Checklist 
---

* Who is the DRI(s)? 
* What is the goal of the topic selected?
* Do you have an ideal month that you would like your topic to be on Twitter Spaces?
* Have you identified the audience? 
* Date: MM/ DD/ YYYY
* Time:
* Region:
* Twitter Space Link:
*  Title of Twitter Space conversation: 
*  Description of topic: 
* Official hashtag: #BeyondCode 

## Add Important Event Details

* Assign this issue to the host and DRI
* Attach to the Beyond Code on Twitter Space [epic](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/1257)
* Create a link on TwitterSpaces
    1. Add to Bambu
* Post on [GitLab’s event page](https://about.gitlab.com/events/)
* Update official [digit assets ](https://www.canva.com/design/DAE7Joehbkk/tDV2qwrlQJmzdMlMG2te6Q/edit#2)
*Submit[ an issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) to tweet about the event
    2. Add to Bambu
* Create the Twitter Space (script doc) [https://docs.google.com/document/d/1PFgbpIe7R_6Ta_P7vqc8qxRbB3LFIH0GN49gR2gatWE/edit#] and guide for day of event. 
* Schedule a short sync with particpates before the live Space. 
* Make sure the bot is connected DRI: @PJMETZ

## Who Are the Participants 

1. Name(s) host include Twitter handle: First Name Last name, @twitterhandle 
1. Name(s) of guest include Twitter handle: (When looking for speakers, it is encouraged to connect with your network and bring in external partners to keep conversations fresh.)
1. Ask for headshots for each participant to be featured in marketing materials


Launching Spaces | Things to know
--- 

* Social team will start the Spaces 
* Each Space can 3 hosts, including two co-hosts and the social team will designate the hosts, who will also have serve as admins of the Spaces 
* The co-hosts will add the guests as speakers 
* Moderator should mention that the Space is being recorded. 
* If you speak your voice and Twitter handle will be part of the recording
* We plan to use the recorder elsewhere 

- If you’d like to speak, you can raise hit the “speaker button”

## Pre-promotion checklist (Social team & DRIs ONLY) 

The social media team creates the Spaces via GitLab’s Twitter account 
1. Adds name of the Spaces 
1. Create social plan
1. Selects topics the Spaces falls under 
1. Creates a calendar invite via Twitter 
1. Share the Spaces URL with the corresponding team (s)
1. Post on [GitLab’s event page](https://about.gitlab.com/events/)

The social media team will push our relevant content ahead of the Spaces 
1. GitLab content: blog, white paper, etc 
1. Issue to keep the conversation going 
1. Third-party content 

Post-event Space
* create the retrospective [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/blob/master/.gitlab/issue_templates/Beyond-Code-Retro) 


```
/label  ~Meetups ~"Evangelist Program"  ~"status:plan"
/assign @jrachel1 @sugaroverflow @award4
/due
