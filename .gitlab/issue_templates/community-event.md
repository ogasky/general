# Community Event Template

<!--
This template is intended for use by members of GitLab's Community Relations team when planning events. If you are planning a meetup, please use our [meetup-organizer](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer) template. 
-->

## Event Info

Please include the following information about your event: 

- Event Name: 
- Event URL: 
- Event Contact Name:
- Event Contact Email:  
- Location: 
- Date(s): 
- Time: 
- Event description: 
- Slack channel, if any: 

## Event Goals (check all that apply)

* [ ]  Brand awareness
* [ ]  Thought leadership
* [ ]  Grow the GitLab community
* [ ]  Attract contributors 
* [ ]  Attract new Open Source projects 
* [ ]  Attract new Education partners 
* [ ]  Diversity & Inclusion
* [ ]  Others (explain): 

## Budget 

*Please include the expected budget for this event and the related finance issue.*

## Community Interaction  
- [ ] There will be a speaker from the GitLab team 
- [ ] There will be a speaker from the wider GitLab community  
- [ ] There will be a GitLab booth/table 
- [ ] There will be strategic meetings with people in the area

## TODOs
<!-- Use this section for additional actions to complete for the event preparation. Feel free to remove if no additional actions are required -->
- [ ] Event TODO `#1`
- [ ] Submit a [social media coverage request](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#requesting-social-posts-)
- [ ] Event TODO `#2`

## Travel and accommodation

*Please include travel (e.g. preferred airports) and accommodation recommendations (e.g. nearby or event discount hotels) to make the coordination and booking for GitLab team members and wider community speakers more effective*

## Team member list

These GitLab team and wider community members will be participating at the event:
<!-- Note: `Role` could be Speaker, Booth duty, Track lead, Attendee, etc.  -->

- `Role:` `@gitlab_id`
- ...

/confidential 
/label ~"Evangelist Program" ~"mktg-status::plan" 
/assign @jrachel1
/due 
