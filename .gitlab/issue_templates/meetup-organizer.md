# Meetup Organizer Template

<!--
Note: this template is intended for organizers from the wider GitLab community who are planning in-person and virtual meetups. 
-->

**For a new organizer**
---

**Some questions to consider before getting started?**

- [ ] Who will be the DRI(s)? 
- [ ] Are you looking to establish a new GitLab meetup group?
- [ ] Is the event you envision would be suited for a meetup or workshop?
- [ ] What is the event cadence for the group (monthly, quarterly, bi-annual)?
- [ ] Have you identified the audience?
- [ ] What is the intention for organizing the group? 

If you are able to provide anwers to each of these questions then you are ready to organize. 

## Event Info

Please include the following information about your event: 

- Event/Group Name: 
- Event/Group URL: 
- Organizer Name: 
- Location (please indicate if `virtual`): 
- Date: 
- Time: 
- Speakers: 
- Description: 

## TODO for Organizer

Please complete the following steps before your event: 

- [ ] Check this box to confirm you acknowledge the following:
   - Meetups are the sole responsibility of the organizer and GitLab shall have no liability in respect of such events.
   - Meetups shall be organised and run in accordance with local guidance and regulations, including public health guidance and regulations relating to the COVID-19 pandemic.
   - Reimbursement of organizer expenses is subject to the [Meetup Expenses Policy](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/#meetup-expenses). 
- [ ] For in-person meetups: Check to indicate you have reviewed the [GitLab Meetups checklist](https://about.gitlab.com/community/meetups/checklist/) and intend to complete all of the necessary steps.
- [ ] For virtual meetups: Check to indicate you have reviewed the [GitLab Virtual Meetups checklist](https://about.gitlab.com/community/meetups/checklist/#planning-a-virtual-meetup) and intend to complete all of the necessary steps.  

## TODO for Evangelist Program Manager 

- [ ] Add to [GitLab Events](https://about.gitlab.com/events/) page.
- [ ] Add FMM, SDR, and SAL for the region to the FYI section so they can communicate the event to nearby accounts. 
- [ ] Ship swag (usually stickers and 3 shirts) and ship to speaker, event organizer, or venue. Confirm shipping instructions and audience size before sending. 
- [ ] Submit [issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) to tweet about the event. 
- [ ] Confirm the number of attendees. 
- [ ] Process expenses. 

FYI: (add folks who may be interested including FMM, SDR and SAL for the territory here) 
/label  ~Meetups ~"Evangelist Program"  ~"status:plan" 
/assign @jrachel1
/due 
