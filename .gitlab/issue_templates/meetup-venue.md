# Meetup Venue Template

<!--
Note: this template is intended for organizations who are seeking to host GitLab meetups. 
-->

## Venue Info

Please include the following information about your venue: 

- Venue:
- Venue URL (or link to photos of venue):
- Address:
- City, State:
- Country:
- Attendance Capacity: 
- Describe the Audio / Visual setup (i.e. will a projector and microphone be available): 
- Would someone from your team like to speak at the event(s) you host?
- Notes: 
