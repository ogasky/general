# GitLab Virtual Meetup

<!--
This template is intended for use by members of GitLab's Community Relations team when planning virtual meetups. If you are a member of the wider GitLab community, please use our [meetup-organizer](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer) template when planning your event. 
-->

## Event Info

Please include the following information about your event: 

- Event Name: 
- Event URL: 
- Event Contact Name:
- Event Contact Email: 
- Location: Virtual 
- Date(s): 
- Time:
- Event description: 


## Event Goals (check all that apply)

* [ ]  Brand awareness
* [ ]  Thought leadership
* [ ]  Grow the GitLab community
* [ ]  Attract contributors 
* [ ]  Attract new Open Source projects 
* [ ]  Attract new Education partners 
* [ ]  Diversity & Inclusion
* [ ]  Others (explain): 

## Budget 

*Please include the expected budget for this event and the related finance issue.*

## Community Interaction  
- [ ] There will be a speaker from the GitLab team 
- [ ] There will be a speaker from the wider GitLab community  
- [ ] There will be a GitLab booth/table 
- [ ] There will be strategic meetings with people in the area

## TODOs
<!-- Use this section for additional actions to complete for the event preparation. Feel free to remove if no additional actions are required -->
- [ ] Find speakers 
- [ ] Create Zoom page
- [ ] Promote on Meetup.com 
- [ ] Add to events.yml
- [ ] Post in forum.gitlab.com
- [ ] Create or update social media issue 
- [ ] Prepare for session 

## Team member list

These GitLab team and wider community members will be participating at the event:
<!-- Note: `Role` could be Speaker, Booth duty, Track lead, Attendee, etc.  -->

- `Organizer:` `@jrachel1`
- ...

/label  ~Meetups ~"Evangelist Program"  ~"mktg-status::plan" 
/assign @jrachel1
/due 
